package homework3.tests;

import homework3.BaseTest;
import homework3.pages.DashboardPage;
import homework3.pages.LoggingPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class Test1 extends BaseTest {

    private LoggingPage loggingPage;
    private DashboardPage dashboardPage;

    @BeforeClass
    public void initPages() {
        loggingPage = new LoggingPage(driver);
        dashboardPage = new DashboardPage(driver);
    }

    @Test
    public void login() throws Exception {
        driver.get(getPropertyValue("url"));

        loggingPage.fillEmailInput(getPropertyValue("username"));
        loggingPage.fillPassInput(getPropertyValue("password"));
        loggingPage.loginButtonClick();
    }

    @Test(dependsOnMethods = "login")
    public void test1() {
        assertEquals("Пульт", dashboardPage.getPageTitle(), "Page title at Dashboard is wrong");


    }

}
