package homework3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class BaseTest {

    private static Properties properties;
    protected EventFiringWebDriver driver;

    private static void initProperties(){
        try {
            FileInputStream Locator = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/project.properties");
            properties = new Properties();
            properties.load(Locator);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPropertyValue(String propertyName) throws Exception {
        return properties.getProperty(propertyName);
    }

    @Parameters("browser")
    @BeforeClass
    public void setUp(String browser){
        initProperties();
        WebDriver webDriver = getDriver(browser);

        webDriver.manage().window().maximize();

        driver = new EventFiringWebDriver(webDriver);
        driver.register(new EventHandler());
    }

    @AfterClass
    public void tierDown(){
        if(driver!=null){
            driver.quit();
        }
    }

    private WebDriver getDriver(String browser) {
        switch (browser) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
                return new ChromeDriver();
            case "ie":
            case "firefox":
                System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/drivers/geckodriver");
                return new FirefoxDriver();
            default:
                System.out.println("Driver for browser '" + browser + "' is absent or there are some problems with it");
                return null;
        }
    }
}
