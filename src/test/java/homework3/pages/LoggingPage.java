package homework3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoggingPage {
    private EventFiringWebDriver driver;

    private By emailInputField = By.id("email");
    private By passwordInputField = By.id("passwd");
    private By loginButton = By.name("submitLogin");

    public LoggingPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void open(){
//        driver.get(Properties.getBaseUrl());
    }

    public void fillEmailInput(String text){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(emailInputField));

        driver.findElement(emailInputField).sendKeys(text);
    }

    public void fillPassInput(String text){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(passwordInputField));

        driver.findElement(passwordInputField).sendKeys(text);
    }

    public void loginButtonClick(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));

        driver.findElement(loginButton).click();
    }
}
