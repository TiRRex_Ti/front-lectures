package homework3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage {
    private EventFiringWebDriver driver;

    private By employeeInfoButton = By.id("employee_infos");
    private By logoutButton = By.id("header_logout");
    private By catalogMenuButton = By.cssSelector(".maintab[data-submenu='9']");
    private By catalogSubMenuCategoriesButton = By.cssSelector("#subtab-AdminCategories");
    private By pageTitle = By.cssSelector(".page-title");


    public DashboardPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void employeeInfoButtonClick() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(employeeInfoButton));
        driver.findElement(employeeInfoButton).click();
    }

    public void logoutButtonClick() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(logoutButton));
        driver.findElement(logoutButton).click();
    }

    public void moveToCatalogMenuButton() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogMenuButton));

//        Actions action = new Actions(driver);
//        action.moveToElement(driver.findElement(catalogMenuButton), 5, 5);
//        action.perform();

        String javaScript = "var evObj = document.createEvent('MouseEvents');" +
                "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
                "arguments[0].dispatchEvent(evObj);";

        ((JavascriptExecutor)driver).executeScript(javaScript, driver.findElement(catalogMenuButton));
    }

    public void catalogSubMenuCategoriesButtonClick(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(catalogSubMenuCategoriesButton));
        driver.findElement(catalogSubMenuCategoriesButton).click();
    }

    public String getPageTitle(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageTitle));

        return driver.findElement(pageTitle).getText();
    }
}
